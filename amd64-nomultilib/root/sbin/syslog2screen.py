#!/usr/bin/env python3

import sys
import socket
import os, os.path

SEVERITY = [
    b'emerg',
    b'alert',
    b'crit',
    b'error',
    b'warn',
    b'notice',
    b'info',
    b'debug'
]

FACILITY = [
    b'kern',
    b'user',
    b'mail',
    b'daemon',
    b'auth',
    b'syslog',
    b'lpr',
    b'news',
    b'uucp',
    b'cron',
    b'authpriv',
    b'ftp',
    b'ntp',
    b'audit',
    b'alert',
    b'clockdaemon',
    b'local0',
    b'local1',
    b'local2',
    b'local3',
    b'local4',
    b'local5',
    b'local6',
    b'local7'
]

def display(syslog_line):
    # search for "<"
    if syslog_line[0] != 60:
        return syslog_line

    # search for ">"
    if syslog_line[3] == 62:
        pri_size = 4
    else:
        pri_size = 5

    pri = int(syslog_line[1:pri_size-1])

    severity = pri % 8
    facility = pri // 8

    sys.stdout.buffer.write(FACILITY[facility])
    sys.stdout.buffer.write(b'.')
    sys.stdout.buffer.write(SEVERITY[severity])
    sys.stdout.buffer.write(b' ')

    sys.stdout.buffer.write(syslog_line[pri_size:])
    sys.stdout.buffer.write(b'\n')

if os.path.exists( "/dev/log" ):
    os.remove( "/dev/log" )

sys.stdout.buffer.write(b'displaying logs from syslog ... \n')
server = socket.socket( socket.AF_UNIX, socket.SOCK_DGRAM )
server.bind("/dev/log")
os.chmod("/dev/log", 0o666)

relay = os.path.exists("/loghost/socket")
if relay:
    client = socket.socket( socket.AF_UNIX, socket.SOCK_DGRAM )
    client.connect( "/loghost/socket" )

while True:
    recieved_bytes = server.recv( 32768 )
    display( recieved_bytes )
    if relay:
        client.send( recieved_bytes )

