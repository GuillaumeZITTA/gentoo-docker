#!/bin/bash

[[ -f /pre_start.sh ]] && /pre_start.sh

stop() {
    kill -9 $sysylog_pid
    openrc shutdown
}

trap stop SIGINT SIGTERM

PYTHONUNBUFFERED=1 syslog2screen.py &
sysylog_pid=$!

sleep 1

/sbin/openrc -v default

while sleep 1;
do
	echo -n ''
done
